package com.korbiak.view;

import com.korbiak.controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class View {

    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private Map<Integer, String> states;
    private static Scanner input;
    private static Logger logger = LogManager.getLogger(View.class);


    private void setMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", "1 - Add new Task.");
        menu.put("2", "2 - Change task state.");
        menu.put("3", "3 - Get information about current tasks.");
        menu.put("Q", "Q - exit");
    }

    public View() {
        controller = new Controller();
        input = new Scanner(System.in);
        setMenu();
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::getEx1);
        methodsMenu.put("2", this::getEx2);
        methodsMenu.put("3", this::getEx3);

        states = new LinkedHashMap<>();
        states.put(1, "ToDoState");
        states.put(2, "InProgresState");
        states.put(3, "CodeReviewState");
        states.put(4, "DoneState");
    }

    private void getEx3() {
        logger.info("Current tasks: \n");
        logger.info(controller.getInfo());
    }

    private void getEx2() {
        logger.info("Enter task index to change: ");
        Scanner scanner = new Scanner(System.in);
        int index = scanner.nextInt();
        logger.info(showStates());
        logger.info("Enter state number: ");
        scanner = new Scanner(System.in);
        int index2 = scanner.nextInt();
        controller.setTaskState(index, index2);

    }

    private void getEx1() {
        logger.info("Enter task to add: ");
        Scanner scanner = new Scanner(System.in);
        String task = scanner.nextLine();
        controller.addTask(task);
    }

    private String showStates() {
        StringBuilder answer = new StringBuilder();
        for (int i = 1; i < states.size() + 1; i++) {
            answer.append(i).append(" ").append(states.get(i)).append("\n");
        }
        return answer.toString();
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            logger.info("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).getCom();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }

    private void outputMenu() {
        logger.info("\nMENU:");
        for (String str : menu.values()) {
            logger.info(str);
        }
    }
}
