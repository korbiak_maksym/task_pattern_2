package com.korbiak.model;

public class State {
    public void addToDo(Task task) {
        System.out.println("Command addToDo is not allowed here!");
    }
    public void toProgress(Task task) {
        System.out.println("Command toProgress is not allowed here!");
    }
    public void toReview(Task task) {
        System.out.println("Command toReview is not allowed here!");
    }
    public void toDone(Task task) {
        System.out.println("Command toDone is not allowed here!");
    }

    @Override
    public String toString() {
        return getClass().getSimpleName();
    }
}

