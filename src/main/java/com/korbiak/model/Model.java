package com.korbiak.model;

import java.util.ArrayList;

public class Model {
    private ArrayList<Task> tasks;

    public Model(){
        tasks = new ArrayList<>();
    }

    public Task getTask(int index){
        return tasks.get(index);
    }

    public void addTask(String taskName){
        tasks.add(new Task(taskName));
    }

    @Override
    public String toString() {
        return tasks.toString();
    }
}
