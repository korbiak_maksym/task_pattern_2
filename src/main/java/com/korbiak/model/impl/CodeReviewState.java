package com.korbiak.model.impl;

import com.korbiak.model.State;
import com.korbiak.model.Task;

public class CodeReviewState extends State {
    @Override
    public void addToDo(Task task) {
        task.setState(new ToDoState());
    }

    @Override
    public void toProgress(Task task) {
        task.setState(new InProgresState());
    }
}
