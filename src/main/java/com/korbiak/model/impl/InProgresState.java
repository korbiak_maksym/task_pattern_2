package com.korbiak.model.impl;

import com.korbiak.model.State;
import com.korbiak.model.Task;

public class InProgresState extends State {

    @Override
    public void toReview(Task task) {
        task.setState(new CodeReviewState());
    }
}
