package com.korbiak.model.impl;

import com.korbiak.model.State;
import com.korbiak.model.Task;

public class ToDoState extends State {
    @Override
    public void toProgress(Task task) {
        task.setState(new InProgresState());
    }
}
