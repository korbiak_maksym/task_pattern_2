package com.korbiak.model;

import com.korbiak.model.impl.ToDoState;

public class Task {

    private static int counter = 0;
    private int taskNum;
    private State state;
    private String taskName;

    public Task(String taskName) {
        counter++;
        taskNum = counter;
        this.taskName = taskName;
        state = new ToDoState();
    }

    public void setState(State state) {
        this.state = state;
    }

    public State getState() {
        return state;
    }

    @Override
    public String toString() {
        return "Task{" +
                "taskNum=" + taskNum +
                ", state=" + state +
                ", TaskName='" + taskName + '\'' +
                '}';
    }
}
