package com.korbiak.controller;

import com.korbiak.model.Model;
import com.korbiak.model.Task;


public class Controller {

    private Model model;

    public Controller() {
        this.model = new Model();
    }

    public void addTask(String taskName) {
        model.addTask(taskName);
    }

    public void setTaskState(int index, int index2) {

        Task task = model.getTask(index - 1);

        switch (index2) {
            case 1:
                task.getState().addToDo(task);
                break;
            case 2:
                task.getState().toProgress(task);
                break;
            case 3:
                task.getState().toReview(task);
                break;
            case 4:
                task.getState().toDone(task);
                break;
        }
    }

    public String getInfo() {
        return model.toString();
    }

}
